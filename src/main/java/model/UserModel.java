package model;

public class UserModel {
    private String username;
    private String password;
    private boolean remember_me;
    private ErrorModel errorModel;

    public UserModel() {

    }

    public UserModel(String username, String password) {
        this.username = username;
        this.password = password;
        this.errorModel = new ErrorModel(false, "");
    }

    public UserModel(String username, String password, boolean remember_me) {
        this.username = username;
        this.password = password;
        this.remember_me = remember_me;
        this.errorModel = new ErrorModel(false, "");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ErrorModel getErrorModel() {
        return errorModel;
    }

    public void setErrorModel(ErrorModel errorModel) {
        this.errorModel = errorModel;
    }

    public boolean isRemember_me() {
        return remember_me;
    }

    public void setRemember_me(boolean remember_me) {
        this.remember_me = remember_me;
    }
}
