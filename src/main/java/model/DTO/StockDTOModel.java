package model.DTO;

import java.util.Date;

public class StockDTOModel {
    private int id;
    private int quantity;
    private Date expireDate;
    private String productName;
    private int productId;
    private double price;

    public StockDTOModel() {

    }

    public StockDTOModel(int id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public StockDTOModel(int id, String productName, int productId, Date expireDate, int quantity, double price) {
        this.id = id;
        this.productName = productName;
        this.productId = productId;
        this.expireDate = expireDate;
        this.quantity = quantity;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
