package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import database.UserDataAccess;
import model.UserModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet({"/login", ""})
public class LoginController extends HttpServlet {
    private ObjectMapper mapper;

    @Override
    public void init() throws ServletException {
        this.mapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.getSession().invalidate();
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            UserModel userModel = mapper.readValue(req.getParameter("user"), UserModel.class);

            UserModel authenticated_userModel = UserDataAccess.authenticate(userModel.getUsername(), userModel.getPassword());

            if (!authenticated_userModel.getErrorModel().isError()) {
                HttpSession oldSession = req.getSession(false);
                if (oldSession != null) {
                    oldSession.invalidate();
                }
                req.getSession(true).setMaxInactiveInterval(1 * 60);
                req.getSession().setAttribute("user", authenticated_userModel);

                if (userModel.isRemember_me()) {
                    Cookie cookie_username = new Cookie("username", userModel.getUsername());
                    cookie_username.setMaxAge(60 * 60 * 24 * 31);
                    resp.addCookie(cookie_username);

                    Cookie cookie_remember_me = new Cookie("remember-me", "true");
                    cookie_remember_me.setMaxAge(60 * 60 * 24 * 31);
                    resp.addCookie(cookie_remember_me);
                } else {
                    Cookie cookie_username = new Cookie("username", null);
                    cookie_username.setMaxAge(0);
                    resp.addCookie(cookie_username);

                    Cookie cookie_remember_me = new Cookie("remember-me", null);
                    cookie_remember_me.setMaxAge(0);
                    resp.addCookie(cookie_remember_me);
                }
            }
            resp.getWriter().print(mapper.writeValueAsString(authenticated_userModel));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}