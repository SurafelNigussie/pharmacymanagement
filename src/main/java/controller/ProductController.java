package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import database.ProductDataAccess;
import model.ProductModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/product")
public class ProductController extends HttpServlet {
    private ObjectMapper mapper;

    @Override
    public void init() throws ServletException {
        this.mapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (request.getParameter("productId") != null) {
                response.getWriter().print(mapper.writeValueAsString(ProductDataAccess.getProduct(Integer.parseInt(request.getParameter("productId")))));
            } else {
                request.setAttribute("products", ProductDataAccess.getAllProducts());
                request.getRequestDispatcher("/product.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            ProductModel productModel = mapper.readValue(req.getParameter("product"), ProductModel.class);
            if (productModel.getId() == 0) {
                resp.getWriter().print(mapper.writeValueAsString(ProductDataAccess.addProduct(productModel)));
            } else {
                resp.getWriter().print(mapper.writeValueAsString(ProductDataAccess.updateProduct(productModel)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            ProductDataAccess.deleteProduct(Integer.parseInt(req.getParameter("productId")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}