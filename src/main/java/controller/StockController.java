package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import database.StockDataAccess;
import model.DTO.StockDTOModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/stock")
public class StockController extends HttpServlet {
    private ObjectMapper mapper;

    @Override
    public void init() throws ServletException {
        this.mapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (request.getParameter("stockId") != null) {
                response.getWriter().print(mapper.writeValueAsString(StockDataAccess.getStock(Integer.parseInt(request.getParameter("stockId")))));
            } else if (request.getParameter("productName") != null) {
                response.getWriter().print(mapper.writeValueAsString(StockDataAccess.getAllStocksByProductName(request.getParameter("productName"))));
            } else {
                request.setAttribute("stocks", StockDataAccess.getAllStocks());
                request.getRequestDispatcher("/stock.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            StockDTOModel stockDTOModel = mapper.readValue(req.getParameter("stock"), StockDTOModel.class);
            if (stockDTOModel != null) {
                resp.getWriter().print(mapper.writeValueAsString(StockDataAccess.updateStock(stockDTOModel)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            StockDataAccess.deleteStock(Integer.parseInt(req.getParameter("stockId")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
