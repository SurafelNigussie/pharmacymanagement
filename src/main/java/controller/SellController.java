package controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import database.SellDataAccess;
import model.DTO.SellDTOModel;
import model.ErrorModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/sell")
public class SellController extends HttpServlet {
    private ObjectMapper mapper;

    @Override
    public void init() throws ServletException {
        this.mapper = new ObjectMapper();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("productName") != null) {
            response.getWriter().print(mapper.writeValueAsString(SellDataAccess.getAllForSellByProductName(request.getParameter("productName"))));
        } else {
            request.setAttribute("sell_items", SellDataAccess.getAllForSellProducts());
            request.getRequestDispatcher("/sell.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<SellDTOModel> sellDTOModels = mapper.readValue(req.getParameter("sell"), new TypeReference<List<SellDTOModel>>() {});
            if (sellDTOModels != null) {
                SellDataAccess.sellProducts(sellDTOModels);
                resp.getWriter().print(mapper.writeValueAsString(new ErrorModel(false, "Successful.")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
