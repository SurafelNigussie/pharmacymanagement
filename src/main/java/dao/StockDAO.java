package dao;

import model.DTO.StockDTOModel;
import model.ProductModel;
import model.StockModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockDAO {
    private final Map<Integer, StockModel> stockModelMap = new HashMap<>();
    private ProductDAO productDAO = new ProductDAO();

    {
        StockModel stockModel1 = new StockModel(generateId(), 20, 1);
        StockModel stockModel2 = new StockModel(generateId(), 20, 2);

        stockModelMap.put(stockModel1.getId(), stockModel1);
    }

    public List<StockDTOModel> getAllStocks() {
        List<StockModel> stockModels = new ArrayList<>(stockModelMap.values());
        return convertStockToDTOs(stockModels);
    }

    public StockDTOModel getStock(Integer id) {
        return convertStockToDTO(stockModelMap.get(id));
    }

    public List<StockDTOModel> getStocksByProductName(String productName) {
        List<StockModel> stockModels = new ArrayList<>();

        for (StockModel stock : stockModelMap.values()) {
            ProductModel productModel = productDAO.getProduct(stock.getProductId());
            if (productModel != null && productModel.getName().contains(productName)) {
                stockModels.add(stock);
            }
        }

        return convertStockToDTOs(stockModels);
    }

    public StockModel addStock(StockModel stockModel) {
        stockModel.setId(generateId());
        stockModelMap.put(stockModel.getId(), stockModel);
        return stockModel;
    }

    public StockDTOModel updateProduct(StockDTOModel stockDTOModel) {
        StockModel updateStockModel = stockModelMap.get(stockDTOModel.getId());
        updateStockModel.setQuantity(stockDTOModel.getQuantity());
        stockModelMap.put(stockDTOModel.getId(), updateStockModel);
        return convertStockToDTO(updateStockModel);
    }

    public void deleteProduct(int stockId) {
        stockModelMap.remove(stockId);
    }

    private Integer generateId() {
        return stockModelMap.size() + 1;
    }

    private List<StockDTOModel> convertStockToDTOs(List<StockModel> stockModels) {
        List<StockDTOModel> stockDTOModels = new ArrayList<>();

        for (StockModel stock :
                stockModels) {
            stockDTOModels.add(convertStockToDTO(stock));
        }

        return stockDTOModels;
    }

    private StockDTOModel convertStockToDTO(StockModel stock) {
        StockDTOModel stockDTOModel = new StockDTOModel();
        ProductModel productModel = productDAO.getProduct(stock.getProductId());
        stockDTOModel.setId(stock.getId());
        stockDTOModel.setProductName(productModel.getName());
        stockDTOModel.setProductId(productModel.getId());
        stockDTOModel.setExpireDate(productModel.getExpireDate());
        stockDTOModel.setQuantity(stock.getQuantity());
        stockDTOModel.setPrice(productModel.getPrice());
        return stockDTOModel;
    }

    private StockModel convertDTOtoStock(StockDTOModel stockDTOModel) {
        StockModel stockModel = new StockModel();
        stockModel.setId(stockDTOModel.getId());
        stockModel.setProductId(stockDTOModel.getProductId());
        stockModel.setQuantity(stockDTOModel.getQuantity());
        return stockModel;
    }
}
