package dao;

import model.ProductModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProductDAO {
    private final Map<Integer, ProductModel> productModelMap = new HashMap<>();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");

    {
        ProductModel product1 = new ProductModel(generateId(), "Syringe 2ml", "2ml injection syringe", 2.99, new Date());
        ProductModel product2 = new ProductModel(generateId(), "Syringe 5ml", "5ml injection syringe", 3.99, new Date());

        productModelMap.put(product1.getId(), product1);
    }

    public List<ProductModel> getAllProducts() {
        return new ArrayList<>(productModelMap.values());
    }

    public ProductModel getProduct(Integer id) {
        return productModelMap.get(id);
    }

    public ProductModel addProduct(ProductModel productModel) {
        productModel.setId(generateId());
        productModelMap.put(productModel.getId(), productModel);
        return productModel;
    }

    public ProductModel updateProduct(ProductModel productModel) {
        ProductModel updateProductModel = productModelMap.get(productModel.getId());
        updateProductModel.setName(productModel.getName());
        updateProductModel.setDescription(productModel.getDescription());
        updateProductModel.setExpireDate(new java.sql.Date(productModel.getExpireDate().getTime()));
        updateProductModel.setPrice(productModel.getPrice());
        productModelMap.put(productModel.getId(), updateProductModel);
        return updateProductModel;
    }

    public void deleteProduct(int productId) {
        productModelMap.remove(productId);
    }

    private Integer generateId() {
        return productModelMap.size() + 1;
    }

    private Date getDate() {
        Date date = new Date();
        String date_string = dateFormat.format(date);
        try {
            date = dateFormat.parse(date_string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}