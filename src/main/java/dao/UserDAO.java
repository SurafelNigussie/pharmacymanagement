package dao;

import model.ErrorModel;
import model.UserModel;

import java.util.HashMap;
import java.util.Map;

public class UserDAO {
    private final Map<String, UserModel> userModelMap = new HashMap<>();

    {
        UserModel user1 = new UserModel("user1", "test");
        UserModel user2 = new UserModel("user2", "test");

        userModelMap.put(user1.getUsername(), user1);
        userModelMap.put(user2.getUsername(), user2);
    }

    public UserModel authenticate(String username, String password) {
        UserModel userModel = userModelMap.get(username);
        if (userModel == null) {
            UserModel unknown_user = new UserModel(username, password);
            unknown_user.setErrorModel(new ErrorModel(true, "Invalid username or password, try again!"));
            return unknown_user;
        } else {
            if (userModel.getUsername().equals(username) && userModel.getPassword().equals(password)) {
                userModel.setErrorModel(new ErrorModel(false, "Login Successful!"));
                return userModel;
            } else {
                userModel.setErrorModel(new ErrorModel(true, "Invalid username or password, try again!"));
                return userModel;
            }
        }
    }
}