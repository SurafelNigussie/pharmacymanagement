package database;

import model.ProductModel;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ProductDataAccess {
    public static ProductModel getProduct(int id) {
        ProductModel productModel = new ProductModel();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select * from `product` where id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                mapToProductModel(rs, productModel);
            }

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productModel;
    }

    public static ProductModel updateProduct(ProductModel productModel) {
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("UPDATE `product` SET `name` = ?, `description` = ?, `price` = ?, `expiredate` = ? WHERE `product`.`id` = ?");
            ps.setString(1, productModel.getName());
            ps.setString(2, productModel.getDescription());
            ps.setDouble(3, productModel.getPrice());
            ps.setDate(4, new java.sql.Date(productModel.getExpireDate().getTime()));
            ps.setInt(5, productModel.getId());
            ps.executeUpdate();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productModel;
    }

    public static List<ProductModel> getAllProducts() {
        List<ProductModel> productModels = new ArrayList<>();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select * from `product`");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ProductModel productModel = new ProductModel();
                mapToProductModel(rs, productModel);
                productModels.add(productModel);
            }

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productModels;
    }

    public static void deleteProduct(int id) {
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps;

            ps = con.prepareStatement("delete from stock where productId = ?");
            ps.setInt(1, id);
            ps.executeUpdate();

            ps = con.prepareStatement
                    ("delete from product where id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ProductModel addProduct(ProductModel productModel) {
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps;

            //Insert product, use that id for stock
            ps = con.prepareStatement
                    ("INSERT INTO `product` (`name`, `description`, `price`, `expiredate`) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, productModel.getName());
            ps.setString(2, productModel.getDescription());
            ps.setDouble(3, productModel.getPrice());
            ps.setDate(4, null);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                productModel.setId(rs.getInt(1));
            }

            //Also, add stock by default
            ps = con.prepareStatement
                    ("INSERT INTO `stock` (`quantity`, `productId`) VALUES (?, ?)");
            ps.setInt(1, 0);
            ps.setInt(2, productModel.getId());
            ps.executeUpdate();

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productModel;
    }

    private static void mapToProductModel(ResultSet rs, ProductModel productModel) throws SQLException {
        productModel.setId(rs.getInt("id"));
        productModel.setName(rs.getString("name"));
        productModel.setDescription(rs.getString("description"));
        productModel.setPrice(rs.getDouble("price"));
        productModel.setExpireDate(rs.getDate("expireDate"));
    }
}
