package database;

import model.ErrorModel;
import model.UserModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDataAccess {
    public static UserModel authenticate(String username, String password) {
        UserModel userModel = new UserModel();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select id, username, password from `user` where username = ? && password = ?");
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();

            if (!rs.next()) {
                con.close();
                UserModel unknown_user = new UserModel(username, password);
                unknown_user.setErrorModel(new ErrorModel(true, "Invalid username or password, try again!"));
                return unknown_user;
            } else {
                con.close();
                userModel.setErrorModel(new ErrorModel(false, "Login Successful!"));
                return userModel;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return userModel;
    }
}
