package database;

import model.DTO.SellDTOModel;
import model.SellModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SellDataAccess {
    public static List<SellModel> getAllForSellProducts() {
        List<SellModel> sellModels = new ArrayList<>();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select * from `stock` s join product p on s.productId = p.id where s.quantity > 0");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                sellModels.add(mapToSellModel(rs));
            }

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sellModels;
    }

    public static void sellProducts(List<SellDTOModel> sellDTOModels) {
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            for (SellDTOModel sellDTOModel :
                    sellDTOModels) {
                PreparedStatement ps = con.prepareStatement
                        ("update stock set quantity = quantity - ? where productId = ?");
                ps.setInt(1, sellDTOModel.getQuantity());
                ps.setInt(2, sellDTOModel.getProductId());
                ps.executeUpdate();
            }
            con.close();
        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    public static List<SellModel> getAllForSellByProductName(String productName) {
        List<SellModel> sellModels = new ArrayList<>();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select * from stock s join product p on s.productId = p.id where p.name LIKE '%" + productName + "%'");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                sellModels.add(mapToSellModel(rs));
            }

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sellModels;
    }

    private static SellModel mapToSellModel(ResultSet rs) throws SQLException {
        SellModel sellModel = new SellModel();
        sellModel.setId(rs.getInt("id"));
        sellModel.setName(rs.getString("name"));
        sellModel.setDescription(rs.getString("description"));
        sellModel.setPrice(rs.getDouble("price"));
        sellModel.setExpireDate(rs.getDate("expiredate"));
        sellModel.setProductId(rs.getInt("productId"));
        return sellModel;
    }
}
