package database;

import model.DTO.StockDTOModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockDataAccess {
    public static List<StockDTOModel> getAllStocks() {
        List<StockDTOModel> stockDTOModels = new ArrayList<>();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select * from `stock` s join product p on s.productId = p.id");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                stockDTOModels.add(mapToStockDTOModel(rs));
            }

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stockDTOModels;
    }

    public static StockDTOModel getStock(int id) {
        StockDTOModel stockDTOModel = new StockDTOModel();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select * from `stock` s join product p on s.productId = p.id where s.id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                stockDTOModel = mapToStockDTOModel(rs);
            }

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stockDTOModel;
    }

    public static List<StockDTOModel> getAllStocksByProductName(String productName) {
        List<StockDTOModel> stockDTOModels = new ArrayList<>();
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("select * from stock s join product p on s.productId = p.id where p.name LIKE '%" + productName + "%'");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                stockDTOModels.add(mapToStockDTOModel(rs));
            }

            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stockDTOModels;
    }

    public static void deleteStock(int id) {
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("delete from stock where id = ?");
            ps.setInt(1, id);
            ps.executeUpdate();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static StockDTOModel updateStock(StockDTOModel stockDTOModel) {
        try {
            Connection con = DatabaseConnection.initializeDatabase();
            PreparedStatement ps = con.prepareStatement
                    ("update stock set quantity = ? where id = ?");
            ps.setInt(1, stockDTOModel.getQuantity());
            ps.setInt(2, stockDTOModel.getId());
            ps.executeUpdate();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stockDTOModel;
    }

    private static StockDTOModel mapToStockDTOModel(ResultSet rs) throws SQLException {
        StockDTOModel stockDTOModel = new StockDTOModel();
        stockDTOModel.setId(rs.getInt("id"));
        stockDTOModel.setQuantity(rs.getInt("quantity"));
        stockDTOModel.setProductId(rs.getInt("productId"));
        stockDTOModel.setExpireDate(rs.getDate("expiredate"));
        stockDTOModel.setProductName(rs.getString("name"));
        stockDTOModel.setPrice(rs.getDouble("price"));
        return stockDTOModel;
    }
}
