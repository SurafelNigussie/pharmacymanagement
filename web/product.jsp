<%--
  Created by IntelliJ IDEA.
  User: Hiwot Bishaw
  Date: 3/15/2019
  Time: 6:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Products</title>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="resources/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
    <script src="resources/js/moment.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/products.js"></script>
    <style>
        form label {
            display: inline-block;
            width: 100px;
        }

        form div {
            margin-bottom: 10px;
        }

        .error {
            color: red;
            margin-left: 5px;
        }

        label.error {
            display: inline;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="product"
        ><i class="flaticon-pharmacy"></i><span>Re</span>Medic</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#ftco-nav" aria-controls="ftco-nav"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="product" class="nav-link">Products</a></li>
                <li class="nav-item"><a href="sell" class="nav-link">Sell</a></li>
                <li class="nav-item"><a href="stock" class="nav-link">Stock</a></li>
                <li class="nav-item"><a href="login" class="nav-link">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container main">
    <div class="row">
        <div class="col-md-4 pr-md-5">
            <h4>Product Form</h4>
            <form action="" method="post" id="form">
                <div class="form-group">
                    <input type="hidden" class="form-control"  name="id" id="id">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                </div>

                <div class="form-group">
                    <input type="date" class="form-control" placeholder="Exp. Date" name="exp_date" id="exp_date">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control" placeholder="Price" name="price" id="price">
                </div>
                <div class="form-group">
                    <textarea name="description" id="desc" cols="30" rows="3" class="form-control"
                              placeholder="Description"></textarea>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-primary py-3 px-5" id="add">
                    <input type="submit" value="Edit" class="btn btn-primary py-3 px-5" id="edit" style="display: none">

                    <button type="reset" class="btn btn-light py-3 px-5" id="cancel">Cancel</button>
                </div>
            </form>

        </div>

        <div class="col-md-8">
            <h4>Products</h4>
            <table class="table table-hover" id="tbl_products">

                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Exp. Date</th>
                    <th scope="col"> Actions</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${products}" var="product">
                    <tr id="<c:out value="${product.id}"/>">
                        <th scope="row"><c:out value="${product.id}"/></th>
                        <td title="<c:out value="${product.description}"/>"><c:out value="${product.name}"/></td>
                        <td><c:out value="${product.price}"/></td>
                        <td><c:out value="${product.expireDate}"/></td>
                        <td>
                            <button class="btn btn-danger"  aria-label="Delete"
                                    id='<c:out value="${product.id }"/>delete'
                                    onclick="removeProduct(this)">
                                <i class="fa fa-trash-o"></i>
                            </button>
                            <button class="btn btn-primary" aria-label="edit"
                                    id='<c:out value="${product.id}"/>edit'
                                    onclick="loadForEdit(this)">
                                <i class="fa fa-edit"></i>
                            </button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
