$(function(){
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
function saveStock(elem){
    const id = parseInt($(elem).attr("id"));
    let stock = {
        id: id,
        quantity: $('#'+id+'input').val()
    };

    $.post('stock', {stock: JSON.stringify(stock)}, function () {
        //reload page, if needed
    }, "json");
}