$(function () {
    $('#submit').click(authenticate);

    function authenticate() {
        var username = $('#username').val();
        var password = $('#password').val();
        var remember_me = $('#remember_me').prop("checked");
        var userObject = {
            username: username,
            password: password,
            remember_me: remember_me
        };

        $.post('login', {user: JSON.stringify(userObject)}, processData, "json");
    }

    function processData(data) {
        if (data.errorModel.error) {
            $('#errorMessage').text("Incorrect Username or Password!");
        } else {
            window.location.href = "/product";
        }
    }
});