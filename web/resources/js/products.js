let submitEdit = false;
$(function () {
    $('#cancel').click(function () {
        $('#edit').css("display", "none");
        $('#add').css("display", "inline");
        submitEdit = false
    });
    $('#form').submit(function(e) {
        let valid = true;

        e.preventDefault();
        $(".error").remove();
        let product = getProductFromForm();
        if(product.name.length < 1){
            $('#name').after('<span class="error">This field is required</span>');
            valid = false;
        }
        if(product.expireDate.length < 1){
            $('#exp_date').after('<span class="error">This field is required</span>');
            valid = false;
        }
        if(product.price.length < 1 || parseInt(product.price) < 0){
            $('#price').after('<span class="error">This field is must be greater than zero</span>');
            valid = false;
        }
        if(valid){
            if(submitEdit){
                editProduct();
                $('#edit').css("display", "none");
                $('#add').css("display", "inline");
                submitEdit = false;
            }
            else{
                addProduct()
            }
            $('#form').get(0).reset();
        }


    })
});

function getProductFromForm() {

    return {
        name: $('#name').val(),
        price: $('#price').val(),
        description:$('#desc').val(),
        expireDate: $('#exp_date').val()
    };
}
function addProduct() {
    let product = getProductFromForm();
    product.id = 0;
    $.post('product', {product: JSON.stringify(product)}, processData, "json")
}

function editProduct() {
    let product = getProductFromForm();
    product.id = $('#id').val();
    $.post('product', {product: JSON.stringify(product)}, processEditedData, "json")

}

function processEditedData(data) {
    const td0 = $('<td>').text(data.id).attr("scope", "row");
    const td1 = $('<td>').text(data.name);
    const td2 = $('<td>').text(data.price);
    const td3 = $('<td>').text(data.expireDate);
    const td4 = $('<td>').html(' <button class="btn btn-danger" href="path/to/settings" aria-label="Delete"' +
        ' id="' + data.id + "edit" + '"\n' +
        ' onclick="removeProduct(this)">\n' +
        '                                <i class="fa fa-trash-o"></i>\n' +
        '                            </button>\n' +
        '                            <button class="btn btn-primary" aria-label="edit" ' +
        'id="' + data.id + "delete" + '"\n' +
        '                                    onclick="loadForEdit(this)">\n' +
        '                                <i class="fa fa-edit"></i>\n' +
        '                            </button>');

    const tr = $('<tr>').attr("id", data.id).append(td0).append(td1).append(td2).append(td3).append(td4);
    $('#'+data.id).replaceWith(tr);
}

function removeProduct(elem) {
    const id = parseInt($(elem).attr("id"));
    $.ajax({
        url: 'product' + '?' + $.param({"productId": id}),
        type: 'DELETE',
        success: function () {
            $("#" + id).remove();
        }
    });

}

function processData(data) {
    //data = JSON.parse(data);
    const td0 = $('<td>').text(data.id).attr("scope", "row");
    const td1 = $('<td>').text(data.name).attr('title', data.description);
    const td2 = $('<td>').text(data.price);
    const td3 = $('<td>').text(data.expireDate);
    const td4 = $('<td>').html(' <button class="btn btn-danger" aria-label="Delete"' +
        ' id="' + data.id + "edit" + '"\n' +
        ' onclick="removeProduct(this)">\n' +
        '                                <i class="fa fa-trash-o"></i>\n' +
        '                            </button>\n' +
        '                            <button class="btn btn-primary" aria-label="edit" ' +
        'id="' + data.id + "delete" + '"\n' +
        '                                    onclick="loadForEdit(this)">\n' +
        '                                <i class="fa fa-edit"></i>\n' +
        '                            </button>');
    var tr = $('<tr>').attr("id", data.id).append(td0).append(td1).append(td2).append(td3).append(td4);
    $('#tbl_products>tbody').append(tr);
}

function loadForEdit(elem) {
    const id = parseInt($(elem).attr("id"));
    $('#add').css("display", "none");
    $('#edit').css("display", "inline");
    submitEdit = true;
    $.ajax({
        url: 'product' + '?' + $.param({"productId": id}),
        type: 'get',
        success: fillFrom
    });

}

function fillFrom(data) {
    data = JSON.parse(data);
    //var date = moment(data.expireDate).format("MM/DD/YYYY").toDate();
    $('#id').val(data.id);
    $('#name').val(data.name);
    $('#exp_date')[0].valueAsDate = new Date(data.expireDate);
    $('#price').val(data.price);
    $('#desc').val(data.description);
}
