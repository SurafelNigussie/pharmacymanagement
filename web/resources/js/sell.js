
$(function(){
    $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
let sellItems = [];

function sell() {
    if(sellItems.length){
        const sellModel = JSON.parse(JSON.stringify(sellItems));
        sellModel.map(function (item) {
            delete item.price;
            return item;
        });

        $.post('sell', {sell: JSON.stringify(sellModel)}, function (data) {
            if (!data.error) {
                printData();
                $('#sellItems').empty();
                sellItems = [];
                $('#total').text("0.00");

            }
        }, "json");
    }

}
function printData()
{
    var divToPrint=document.getElementById("printTable");
    newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}
function updateTotal() {
    const total = sellItems.reduce((a, b) => {
        return a + (b.quantity * b.price);
    }, 0);
    $('#total').text(parseFloat(total).toFixed(2));
}

function addToSell(elem) {
    let elemId = $(elem).attr("id");
    const id = parseInt(elemId);
    if ($('#' + id + 'item').length) {
        let quantityInput = $('#' + elemId + 'quantity');
        let quantity = parseInt(quantityInput.val());
        quantityInput.val(quantity + 1);
        let foundIndex = sellItems.findIndex(x => x.productId === id);
        sellItems[foundIndex].quantity = quantity + 1;
        updateTotal();

    } else {
        $.ajax({
            url: 'product' + '?' + $.param({"productId": id}),
            type: 'get',
            success: addToTable
        });
    }

}

function addToTable(data) {

    data = JSON.parse(data);
    const td0 = $('<td>').text(data.name);
    const td1 = $('<td>').html('  <input type="number"  placeholder="1" ' +
        'id="' + data.id + "quantity" + '"\n' +
        'onchange="onQuantityValueChange(this)" ' +
        ' value="1">\n');
    const td2 = $('<td>').text(data.price).attr("id", data.id + 'price');
    const td3 = $('<td>').html(' <button class="btn btn-danger"  aria-label="Delete"' +
        ' id="' + data.id + "remove" + '"\n' +
        ' onclick="removeProduct(this)">\n' +
        '                                <i class="fa fa-trash-o"></i>\n' +
        '                            </button>\n'
    );
    const tr = $('<tr>').attr("id", data.id + 'item').append(td0).append(td1).append(td2).append(td3);
    $('#sellItems').append(tr);
    sellItems.push({
        'productId': data.id,
        'price': data.price,
        'quantity': 1
    });
    updateTotal();
}

function onQuantityValueChange(elem) {
    elem = $(elem);

    let id = parseInt(elem.attr("id"));
    let quantity = $('#' + id + 'quantity');
    if (quantity.val() < 1) {
        quantity.val(1);
    } else {
        let foundIndex = sellItems.findIndex(x => x.productId === id);
        sellItems[foundIndex].quantity = parseInt(elem.val());
        updateTotal();
    }

}

function removeProduct(elem) {
    const id = parseInt($(elem).attr("id"));
    $("#" + id + 'item').remove();
    sellItems.splice(sellItems.findIndex(x => x.productId === id), 1);
    updateTotal()

}
