<%--
  Created by IntelliJ IDEA.
  User: HiwotBishaw
  Date: 3/16/2019
  Time: 7:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Stock</title>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="resources/js/jquery.js"></script>
    <script src="resources/js/moment.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/stock.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="product"
        ><i class="flaticon-pharmacy"></i><span>Re</span>Medic</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#ftco-nav" aria-controls="ftco-nav"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a href="product" class="nav-link">Products</a></li>
                <li class="nav-item"><a href="sell" class="nav-link">Sell</a></li>
                <li class="nav-item active"><a href="stock" class="nav-link">Stock</a></li>
                <li class="nav-item"><a href="login" class="nav-link">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container main">
    <div class="row">
        <div class="col-md-12">
            <h4>Stock Items</h4>
            <div class="input-group mb-3">

                <input type="text" class="form-control" placeholder="Search..." aria-label="Search"
                       aria-describedby="basic-addon1" id="search">
            </div>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Stock Amount</th>
                    <th scope="col">Unit Price</th>
                    <th scope="col">Exp. Date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody id="myTable">
                <c:forEach items="${stocks}" var="stock">
                    <tr id="<c:out value="${stock.id}"/>">
                        <th scope="row" ><c:out value="${stock.id}"/></th>
                        <td><c:out value="${stock.productName}"/></td>
                        <td>
                            <input type="text"  placeholder="1" aria-label="Search"
                                   aria-describedby="basic-addon1"
                                   id='<c:out value="${stock.id}"/>input'
                                   value='<c:out value="${stock.quantity}"/>'>
                        </td>
                        <td><c:out value="${stock.price}"/></td>
                        <td><c:out value="${stock.expireDate}"/></td>
                        <td>
                            <button class="btn btn-primary" aria-label="save"
                                    id='<c:out value="${stock.id}"/>save'
                                    onclick="saveStock(this)">
                                Save
                            </button>
                        </td>
                    </tr>
                </c:forEach>


                </tbody>

            </table>
        </div>

    </div>
</div>
</body>
</html>
