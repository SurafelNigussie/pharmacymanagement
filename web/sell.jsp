<%--
  Created by IntelliJ IDEA.
  User: HiwotBishaw
  Date: 3/17/2019
  Time: 1:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Sell</title>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="resources/js/jquery.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/sell.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="product"><i class="flaticon-pharmacy"></i><span>Re</span>Medic</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#ftco-nav" aria-controls="ftco-nav"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item "><a href="product" class="nav-link">Products</a></li>
                <li class="nav-item active"><a href="sell" class="nav-link">Sell</a></li>
                <li class="nav-item"><a href="stock" class="nav-link">Stock</a></li>
                <li class="nav-item"><a href="login" class="nav-link">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container main">
    <div class="row">
        <div class="col-md-6 pr-md-5">
            <h4>Selling Items</h4>
            <div class="form-group pull-left">
                <input type="submit" value="Sell" onclick="sell()"
                       class="btn btn-primary">
            </div>
            <table class="table table-sm" id="printTable">
                <thead>
                <tr>

                    <th scope="col">Name</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Price</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody id="sellItems">

                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3"><h3>Total</h3></td>
                    <td colspan="2"><h3 id="total">00.00</h3></td>
                </tr>
                </tfoot>
            </table>

        </div>

        <div class="col-md-6">

                <h4>Products</h4>




        <div class="input-group mb-3">

            <input type="text" class="form-control" placeholder="Search..." aria-label="Search"
                   aria-describedby="basic-addon1" id="search">
        </div>
        <table class="table table-hover" >

            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Price</th>
                <th scope="col">Exp. Date</th>
                <th scope="col"> Actions</th>
            </tr>
            </thead>
            <tbody id="myTable">
            <c:forEach items="${sell_items}" var="product">
                <tr>
                    <th scope="row"><c:out value="${product.id}"/></th>
                    <td title="<c:out value="${product.description}"/>"><c:out value="${product.name}"/></td>
                    <td><c:out value="${product.price}"/></td>
                    <td><c:out value="${product.expireDate}"/></td>
                    <td>
                        <button class="btn btn-primary" aria-label="Add to sell"
                                id='<c:out value="${product.productId }"/>'
                                onclick="addToSell(this)">
                            <i class="fa fa-plus"></i>
                        </button>
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </table>
    </div>
</div>
</div>
</body>
</html>
