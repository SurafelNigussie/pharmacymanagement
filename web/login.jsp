<%--
  Created by IntelliJ IDEA.
  User: SurafelNigussie
  Date: 3/15/2019
  Time: 6:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="ct" uri="http://cs.mum.edu" %>
<%!
    private String getUserName(HttpServletRequest request) {
        String _username = "";
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("username")) {
                    _username = cookie.getValue();
                }
            }
        }
        return _username;
    }

    private String isRememberMe(HttpServletRequest request) {
        String _remember_me = "";
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equals("remember_me")) {
                    if (cookie.getValue().equals("true"))
                        _remember_me = "checked=\"checked\"";
                }
            }
        }
        return _remember_me;
    }
%>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Login</title>
    <link rel="shortcut icon" type="image/png" href="favicon.ico"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet"/>
    <link href="resources/css/bootstrap.min.css"/>
    <link href="resources/css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="resources/css/style.css"/>

    <script src="resources/js/jquery.js"></script>
    <script src="resources/js/authenticate.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="hero-wrap" style="background-image: url('resources/images/bg_6.jpg'); background-attachment:fixed;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-4  text-center fadeInUp ">
                <div id="login">
                    <h3 class="text-white">Login</h3>
                    <div class="form-group">
                        <label for="username" class="text-black">User Name</label>
                        <input type="text" class="form-control" id="username" name="username"
                               value="<%=getUserName(request)%>">
                    </div>
                    <div class="form-group">
                        <label for="password" class="text-black">Password</label>
                        <input type="password" class="form-control" id="password" name="username">
                    </div>
                    <div class="form-group rememberme">
                        <label><input id="remember_me" type="checkbox"  <%=isRememberMe(request)%>> Remember me</label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-secondary" id="submit">
                            Login
                        </button>
                    </div>


                    <label id="errorMessage" class="text-white"></label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sticker">
    <ct:currentDateTime color="red" size="12px"/>
</div>
</body>
</html>
